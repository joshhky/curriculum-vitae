import {NgxLoggerLevel} from 'ngx-logger';

export const environment = {

  /** If we are running in a production or development environment or locally. */
  production: true,

  /** The minimum logging level before printing to console. */
  loggingLevel: NgxLoggerLevel.WARN,

  /** The suffix of the title to append after the current page name. */
  titleSuffix: 'Seth Falco · CV'
};
