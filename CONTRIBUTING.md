# Contributing 
I'm not sure if this is required for a repository like this. ![pandaThink](assets/pandaThink.png)  
This will just set out some guidelines on what contributions are appropriate and what aren't.

## Issues
Issues are always appreciated, I know I'm not perfect and nothing I write is either.
If you encounter a problem on the website, or just think you can provide some constructive 
feedback, I implore you to make an issue and let me know what you're thinking!

## Merge Requests
If you're a strange person that would do a merge request on someone's CV... ![pandaBlank](assets/pandaBlank.png)  

Merge requests are disabled in this repository as this is meant to be a representation
of my skills. Thus, I must have a deep understanding of any problems, 
the solution, and the implementation of any code that is this repository. 
If you notice a problem, I would appreciate you made an issue instead!
