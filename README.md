<div align="center">

# Seth Falco
[![Discord]][discord-guild] [![Build]][gitlab] [![Coverage]][gitlab]
</div>

## About
First to address some ambiguity...
My preferred name is Seth Falco, but legally, I'm actually [Syed Shah].
I will file an enrolled [deed poll] as soon as I can, but I can't now
as I'm not a UK resident at the moment.

This repository is just an interactive version of my CV / resumé. It's a bit more 
flexible than the dull piece of paper I hand out which has no control. This will
provide an easier way to get the amount of detail a potential employer is actually
interested in and filter out the crap they don't need.

If you're here as a potential employer, then I hope you aren't disappointed!

If you're anyone else, then I have no idea why you're here, but if anything
about this project appeals to you, it's open-source so feel free to do
whatever you want!

## Requirements
When creating this project there were a set of requirements I set which may have
created limitations from what I'd like to show case normally in a project:
* Zero Cost: I specifically tried to avoid doing anything that may incur costs
like requiring a server, use of cloud functions, or things with licensing fees.
* Open-Source: I've tried to minimize use of proprietary software and services for development
and deployment.
* Internationalization: Normally I'd internationalize anything I make, but this website
will only be available in languages I'm fluent in... which is only English.

## Open-Source
This project is open-source under the [Apache 2.0]!  
While not legal advice, you can find a [TL;DR] that sums up what you're
allowed and not allowed to do along with any requirements if you want
to use or derive work from this repository.  

[discord-guild]: https://discord.com/invite/hprGMaM "Discord Invite"
[gitlab]: https://gitlab.com/Elypia/elypia-website/commits/master "Repository on GitLab"
[Syed Shah]: https://beta.companieshouse.gov.uk/officers/JHIszHq0eErcApZ9k_FhzBFPF8A/appointments
[deed poll]: https://www.gov.uk/change-name-deed-poll
[NodeJS 12.15.0]: https://nodejs.org/en/ "NodeJS"
[Angular 9+]: https://angular.io "Angular CLI"
[Apache 2.0]: https://www.apache.org/licenses/LICENSE-2.0 "Apache 2.0 License"
[TL;DR]: https://tldrlegal.com/license/apache-license-2.0-(apache-2.0) "TL;DR of Apache 2.0"

[Discord]: https://discord.com/api/guilds/184657525990359041/widget.png "Discord Shield"
[Build]: https://gitlab.com/SethiPandi/curriculum-vitae/badges/master/pipeline.svg "GitLab Build Shield"
[Coverage]: https://gitlab.com/SethiPandi/curriculum-vitae/badges/master/coverage.svg "GitLab Coverage Shield"
